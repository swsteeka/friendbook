# frozen_string_literal: true

class Category < ApplicationRecord #:nodoc:
  has_many :posts
  validates :name, presence: true, uniqueness: true
end
