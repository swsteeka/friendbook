# frozen_string_literal: true

class Post < ApplicationRecord #:nodoc:
  has_attached_file :postphotos,
                    styles: { medium: '300x300>', thumb: '100x100>' },
                    default_url: '/images/:style/missing.png'

  validates_attachment_content_type :postphotos,
                                    content_type: %r{\Aimage/.*\z}

  validates :content, presence: true
  belongs_to :user
  belongs_to :category
  has_many :likes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :notifications, class_name: 'Notification',
                           foreign_key: 'second_target', dependent: :destroy
end
