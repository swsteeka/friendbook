# frozen_string_literal: true

class Notification < ActiveRecord::Base #:nodoc:
  include Notifications::Model
  paginates_per 12
end
