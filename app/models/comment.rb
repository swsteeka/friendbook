# frozen_string_literal: true

class Comment < ApplicationRecord #:nodoc:
  belongs_to :post
  belongs_to :user
  validates :body, presence: true
  after_commit :create_notifications, on: [:create]

  private

  def create_notifications
    Notification.create! do |notification|
      notification.notify_type = 'post'
      notification.actor = user
      notification.user = post.user
      notification.target = self
      notification.second_target = post
    end
  end
end
