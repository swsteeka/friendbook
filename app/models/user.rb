# frozen_string_literal: true

class User < ApplicationRecord #:nodoc:
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: %i[google_oauth2 facebook]
  validates :avatar, presence: true
  has_attached_file :avatar, styles: { medium: '300x300>', thumb: '100x100>' },
                             default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: %r{\Aimage/.*\z}

  scope :search_result, lambda { |search|
    where('name LIKE ?', "#{search}%")
  }

  def self.from_omniauth(auth)
    where(provider: auth.provider, email: auth.info.email)
      .first_or_create do |u|
      u.uid = auth.uid
      u.password = auth.uid
      u.name = auth.info.name
      u.avatar = URI.parse(auth.info.image) if auth.info.image?
    end
  end
  has_many :likes, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_friendship
end
