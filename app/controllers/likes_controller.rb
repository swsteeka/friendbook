# frozen_string_literal: true

class LikesController < ApplicationController #:nodoc:
  def index; end

  def create
    if already_liked
      flash[:notice] = "You can't like more than once"
    else
      @like = Like.create(like_params)
      redirect_to root_path
    end
  end

  def destroy
    Like.find(params[:id]).destroy
    redirect_to root_path
  end

  private

  def already_liked
    Like.where(like_params).exists?
  end

  def like_params
    params.permit(:post_id).merge(user_id: current_user.id)
  end
end
