# frozen_string_literal: true

class NotificationsController < ApplicationController #:nodoc:
  def index
    @notifications = Notification.order('created_at desc').page(params[:page])
  end
end
