# frozen_string_literal: true

class UserlistsController < ApplicationController #:nodoc:
  def index
    friend_suggestion
    @post = Post.new
    friends_posts
    current_user_notifications
  end

  def show
    @user = User.find(params[:id])
    @posts = Post.where(user_id: @user.id)
    @post = Post.new
  end

  private

  def friend_suggestion
    @users = User.where.not(id: current_user.id)
  end

  def friends_posts
    @posts = current_user.friends.map(&:posts).push(current_user.posts).flatten
  end

  def current_user_notifications
    @notifications =
      Notification.where(user_id: current_user).order('created_at DESC')
  end
end
