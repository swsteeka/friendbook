# frozen_string_literal: true

class CategoriesController < ApplicationController #:nodoc:
  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    respond_to do |format|
      create_category(format)
    end
  end

  private

  def create_category(format)
    if @category.save
      format.html do
        redirect_to root_path, notice: 'category was successfully created.'
      end
    else
      format.html { render :new }
      format.json do
        render json: @category.errors, status: :unprocessable_entity
      end
    end
  end

  def category_params
    params.require(:category).permit(:name)
  end
end
