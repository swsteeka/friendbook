# frozen_string_literal: true

class PostsController < ApplicationController #:nodoc:
  def index
    @posts = Post.all
  end

  def create
    @post = Post.new(post_params)
    respond_to do |format|
      if @post.save
        format.js
        flash[:success] = 'post created'
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @post = Post.find(params[:id])
    update_unread_notification_to_read
  end

  def destroy
    @post = Post.find(params[:id])
    @check = find_check_user_post_id
    respond_to do |format|
      destroy_post(format)
    end
  end

  def find_check_user_post_id
    user_id = Post.find(params[:id]).user_id
    current_user.id == user_id
  end

  private

  def destroy_post(format)
    if @check
      @post.destroy
      format.js
      flash[:success] = 'Your post has been deleted'
    else
      flash[:alert] = 'Sorry, post cannot be deleted by unauthorized user.'
    end
  end

  def post_params
    params.require(:post)
          .permit(:title, :content, :category_id, :postphotos)
          .merge(user_id: current_user.id)
  end

  def update_unread_notification_to_read
    notification = Notification.find(params[:notification_id])
    notification.update(read_at: Time.now)
  end
end
