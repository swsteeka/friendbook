# frozen_string_literal: true

class SearchUsersController < ApplicationController #:nodoc:
  def index
    if params[:search_user].blank?
      flash[:alert] = 'Empty Field'
      redirect_to root_path
    else
      @users = User.search_result(params[:search_user])
      render 'searchuser/index'
    end
  end
end
