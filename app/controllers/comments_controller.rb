# frozen_string_literal: true

class CommentsController < ApplicationController #:nodoc:
  def create
    @post = Post.find(params[:post_id])
    respond_to do |format|
      format.js if @post.comments.create(comment_params)
    end
    # redirect_back(fallback_location: root_path)
  end

  def view_previous_comments
    @post = Post.find(params[:post_id])
    @comments = @post.comments.last(params[:offset])
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @check = destroy_associated_comments
    respond_to do |format|
      comment_destroy(format)
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body).merge(user_id: current_user.id)
  end

  def destroy_associated_comments
    if current_user.id == find_user_post_id ||
       current_user.id == find_user_comment_id
      true
    else
      false
    end
  end

  def find_user_post_id
    Post.find(params[:post_id]).user_id
  end

  def find_user_comment_id
    Comment.find(params[:id]).user_id
  end

  def comment_destroy(format)
    if @check
      @comment.destroy
      format.js
      flash[:alert] = 'Comment Deleted'
    else
      format.html do
        redirect_to root_path,
                    success: 'Comment cannot be deleted by unauthorized'
      end
    end
  end
end
