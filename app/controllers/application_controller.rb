# frozen_string_literal: true

class ApplicationController < ActionController::Base #:nodoc:
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  add_flash_types :success, :danger, :info
  # after_action :flash_me

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,
                                      keys:
                                      %i[name avatar])
  end

  # def flash_me
  #     redirect_to root_path, danger: 'Hello'
  #  end
end
