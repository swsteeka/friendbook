# frozen_string_literal: true

class FriendshipsController < ApplicationController #:nodoc:
  def request_friend
    requested_by = current_user
    requested_to = User.find(params[:user_id])
    requested = requested_by.friend_request(requested_to)
    if requested
      flash[:success] = 'Friend requested sent'
    else
      flash[:alert] = 'Request already sent.'
    end
    redirect_to root_path
  end

  def accept_request
    requested_to = current_user
    requested_by = User.find(params[:friend_id])
    accepted = requested_to.accept_request(requested_by)
    if accepted
      flash[:success] = 'Accepted Request'
    else
      flash[:alert] = 'error'
    end
    redirect_to root_path
  end

  def decline_request
    requested_to = current_user
    requested_by = User.find(params[:requested_id])
    declined = requested_to.decline_request(requested_by)
    flash[:alert] = 'Friend request declined' if declined
    redirect_to root_path
  end

  def unfriend
    enemy = current_user
    enemy2 = User.find(params[:user_id])
    unfriended = enemy.remove_friend(enemy2)
    flash[:success] = 'Unfriended' if unfriended
    redirect_to root_path
  end

  def block
    blocker = current_user
    friend = User.find(params[:user_id])
    if current_user.pending_friend_ids.exclude?(friend)
      blocker.friend_request(friend)
    end
    blocker.block_friend(friend)
    flash[:success] = 'Blocked'
    redirect_to root_path
  end
end
