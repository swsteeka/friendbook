# frozen_string_literal: true

class AddAttachmentPostphotosToPosts < ActiveRecord::Migration[5.2] #:nodoc:
  def self.up
    change_table :posts do |t|
      t.attachment :postphotos
    end
  end

  def self.down
    remove_attachment :posts, :postphotos
  end
end
