# frozen_string_literal: true

# Use this hook to configure devise mailer, warden hooks and so forth.
# Many of these configuration options can be set straight in your model.
Devise.setup do |config|
  config.mailer_sender =
    'please-change-me-at-config-initializers-devise@example.com'
  require 'devise/orm/active_record'
  config.case_insensitive_keys = [:email]
  config.strip_whitespace_keys = [:email]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 11
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 6..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
  client_id =
    '1000623160256-n2hnmjrp5jiftp2cp432lr33e9cbuaec.apps.googleusercontent.com'
  client_secret_id = '8Z2qrXVRZcQ9h3VgsZd6XEAT'
  config.omniauth :google_oauth2, client_id, client_secret_id
  fb_app_id = '751148322068504'
  fb_app_secret =
    '982c3b17e783d6253b34153a596b76a4'
  config.omniauth :facebook, fb_app_id, fb_app_secret,
                  secure_image_url: true,
                  callback_url: 'http://localhost:3000/users/auth/facebook/callback'
end
