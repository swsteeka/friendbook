# frozen_string_literal: true

Rails.application.routes.draw do
  # mount Notifications::Engine => '/notifications'
  devise_for :users, controllers:
      { omniauth_callbacks: 'users/omniauth_callbacks' }
  resources :categories
  resources :posts do
    resources :comments
    resources :likes
  end
  resources :userlists
  post 'friend_request' => 'friendships#request_friend'
  post 'accept_friend' => 'friendships#accept_request'
  delete 'decline_request' => 'friendships#decline_request'
  delete 'unfriend' => 'friendships#unfriend'
  put 'block_friend' => 'friendships#block'
  get 'view_previous_comments' => 'comments#view_previous_comments'
  resources :notifications
  resources :search_users, only: [:index]
  root to: 'userlists#index'
end
